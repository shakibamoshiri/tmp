#!/bin/bash

# assign used entered info
TO="$1" # Should contain the value of {ALERT.SENDTO}
FROM="3000458089" # Should contain your Ghasedak phone number
TEXT="$2" # Should container the value of {ALERT.MESSAGE} or {ALERT.SUBJECT}

# send the message using curl
curl -sL -X POST "http://api.ghasedak.me/v2/sms/send/simple" \
	-H "apikey: ${SMS_API_KEY}" \
	-H 'cache-control: no-cache' \
	-H 'content-type: application/x-www-form-urlencoded' \
	-d "message=${TEXT}&linenumber=${FROM}&receptor=${TO}";

# end a new line to deliver log
echo;
